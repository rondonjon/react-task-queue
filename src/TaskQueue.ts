export type TaskQueueListener = () => void

export type TaskRunner<T, R = void> = (task: T) => Promise<R>

type PromisedTask<T, R = void> = {
  task: T
  resolve: (result: R) => void
  reject: (reason?: any) => void
}

export class TaskQueue<T, R = void> {
  private _currentTask: PromisedTask<T, R> | undefined
  private _queuedTasks: PromisedTask<T, R>[] = []
  private _listeners: TaskQueueListener[] = []
  private _error: Error | undefined

  constructor(private run: TaskRunner<T, R>) {}

  listen = (listener: TaskQueueListener) => {
    this._listeners.push(listener)
  }

  unlisten = (listener: TaskQueueListener) => {
    const index = this._listeners.indexOf(listener)
    if (index !== -1) {
      this._listeners.splice(index, 1)
    }
  }

  add = (task: T) => {
    return new Promise<R>((resolve, reject) => {
      this._queuedTasks.push({
        reject,
        resolve,
        task,
      })
      this.notify()
      this.check()
    })
  }

  resume = () => {
    if (this._error) {
      this._error = undefined
      this._currentTask = undefined
      this.notify()
      this.check()
    }
  }

  private notify = () => {
    for (let i = this._listeners.length - 1; i >= 0; i--) {
      this._listeners[i]()
    }
  }

  private check = () => {
    if (this._queuedTasks.length && !this._currentTask && !this._error) {
      const { task, resolve, reject } = (this._currentTask = this._queuedTasks.shift())
      this.notify()

      this.run(task)
        .then((result) => {
          this._currentTask = undefined
          resolve(result)
        })
        .catch((err) => {
          this._error = err instanceof Error ? err : new Error("" + err)
          reject(this._error)
        })
        .finally(() => {
          this.notify()
          this.check()
        })
    }
  }

  get currentTask(): T | undefined {
    return this._currentTask?.task
  }

  get queuedTasks(): T[] {
    return this._queuedTasks.map((t) => t.task)
  }

  get error(): Error | undefined {
    return this._error
  }
}
