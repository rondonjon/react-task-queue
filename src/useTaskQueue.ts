import { useEffect, useMemo, useState } from "react"
import { TaskQueue } from "./TaskQueue"

type State<T, R = void> = {
  add: (newTask: T) => Promise<R>
  resume: () => void
  currentTask: T | undefined
  queuedTasks: T[]
  error: Error | undefined
}

export const useTaskQueue = <T, R = void>(queue: TaskQueue<T, R>): State<T, R> => {
  const [state, setState] = useState<State<T, R>>({
    add: queue.add,
    resume: queue.resume,
    currentTask: queue.currentTask,
    queuedTasks: queue.queuedTasks,
    error: queue.error,
  })

  useEffect(() => {
    const update = () => {
      setState({
        add: queue.add,
        resume: queue.resume,
        currentTask: queue.currentTask,
        queuedTasks: queue.queuedTasks,
        error: queue.error,
      })
    }

    update()

    queue.listen(update)

    return () => {
      queue.unlisten(update)
    }
  }, [queue])

  return state
}
