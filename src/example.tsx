import { createRoot } from "react-dom/client"
import React, { useCallback, useRef, useState } from "react"
import { TaskQueue, TaskRunner } from "./TaskQueue"
import { useTaskQueue } from "./useTaskQueue"

type ExampleTask = {
  id: number
  shouldFail: boolean
}

type ExampleResult = number

const App = () => {
  const sequence = useRef(0)
  const [log, setLog] = useState("")

  // the runner could be defined anywhere, also globally or in a React context
  const runner: TaskRunner<ExampleTask, ExampleResult> = useCallback((task: ExampleTask) => {
    setLog((log) => log + `Starting: #${task.id}\n`)

    return new Promise<ExampleResult>((resolve, reject) => {
      setTimeout(() => {
        if (task.shouldFail) {
          reject(new Error("This job was doomed to fail"))
        } else {
          resolve(Math.random())
        }
      }, 2000)
    })
  }, [])

  // the queue could be defined anywhere, also globally or in a React context
  const queue = useRef(new TaskQueue<ExampleTask, ExampleResult>(runner)).current

  // this is how to access a queue in a component
  const { add, currentTask, error, queuedTasks, resume } = useTaskQueue(queue)

  const addLogging = useCallback(
    (newTask: ExampleTask) => {
      setLog((log) => log + `Adding: #${newTask.id}\n`)

      add(newTask)
        .then((result) => {
          setLog((log) => log + `Finished: #${newTask.id} (result: ${result})\n`)
        })
        .catch((err) => {
          setLog((log) => log + `Failed: #${newTask.id} (${err})\n`)
        })
    },
    [queue]
  )

  const handleAddGood = useCallback(() => {
    addLogging({
      id: ++sequence.current,
      shouldFail: false,
    })
  }, [addLogging])

  const handleAddBad = useCallback(() => {
    addLogging({
      id: ++sequence.current,
      shouldFail: true,
    })
  }, [addLogging])

  return (
    <div className="example-app">
      <div>
        <h1>Demo Page</h1>
        <h2>
          <a href="https://gitlab.com/rondonjon/react-task-queue">rondonjon's react-task-queue library</a>
        </h2>
        <p>Current Task: {currentTask ? "#" + currentTask.id : "(idle)"}</p>
        <p>Queued Tasks: {queuedTasks.length}</p>
        <p>Error: {error?.message}</p>
        <p className="buttons">
          <button onClick={handleAddGood}>Add good Task</button>
          <button onClick={handleAddBad}>Add bad Task</button>
          <button onClick={resume} disabled={!error}>
            Resume
          </button>
        </p>
      </div>
      <textarea className="log" readOnly value={log} />
    </div>
  )
}

const root = createRoot(document.getElementById("root"))
root.render(<App />)
