# rondonjon-react-task-queue

A zero-dependency, lightweight implementation of a Task Queue class and
a `useTaskQueue` React hook which exposes the operations and the current
state of an instance to React components.

Packaged for ESM and CommonJS. Type definitions included.

Usage:

- define a custom, serializable task type
- define a custom runner which executes such tasks (async) and either resolves or rejects a promise
- add tasks to the queue; execution starts immediately and sequentially (FIFO)
- optional: in the event of an error, process the error message and/or resume execution

## Example / Demo

Source Code: `src/example.tsx`

Live: https://rondonjon.gitlab.io/react-task-queue

## Reference

```ts
type TaskQueueListener = () => void

type TaskRunner<T, R = void> = (task: T) => Promise<R>

class TaskQueue<T, R = void> {
  private run
  private _currentTask
  private _queuedTasks
  private _listeners
  private _error
  constructor(run: TaskRunner<T, R>)
  listen: (listener: TaskQueueListener) => void
  unlisten: (listener: TaskQueueListener) => void
  add: (task: T) => Promise<R>
  resume: () => void
  private notify
  private check
  get currentTask(): T | undefined
  get queuedTasks(): T[]
  get error(): Error | undefined
}

type State<T, R = void> = {
  add: (newTask: T) => Promise<R>
  resume: () => void
  currentTask: T | undefined
  queuedTasks: T[]
  error: Error | undefined
}

const useTaskQueue: <T, R = void>(queue: TaskQueue<T, R>) => State<T, R>
```

## Copyright

Copyright (c) 2022 "Ron Don Jon".

All rights reserved.

MIT-Licensed.

## History

| Version | Changes                                                           |
| ------- | ----------------------------------------------------------------- |
| 1.0.0   | Initial version                                                   |
| 1.1.0   | Add `add` and `resume` queue functions to `useTaskQueue()` result |
| 1.2.0   | Support a custom task return type `R`                             |
| 1.2.1   | Prevent resizing of the log output in the example                 |
| 1.2.2   | Improve message texts in the example                              |
